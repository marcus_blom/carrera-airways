Carrera Airways
=

![Screenshot 01](http://marcusblom.se/carrera/01.png)

General
-

Carrera Airways campaign is designed as a generic competition where visitors open the site and enter their personal information to compete. The campaign site is developed to be viewed on mobile devices as well as large screens.

[Click here for a live preview](http://ec2-54-246-53-15.eu-west-1.compute.amazonaws.com/)

The live preview is run on a Amazon EC2 micro instance with the basic LAMP configuration.

Technical overview
-

The site is built with PHP upon the CakePHP framework with a mySQL database and is version controlled with GIT. The CSS framework Skeleton is used for a responsive and mobile friendly experience. The CSS framework Animate is used for some shiny animated transitions. A jQuery plugin called bgstrecher is used for the background slideshow. The background slideshow scales to always fill the whole viewport. Some custom js is written to ensure that the page loads before playing the animated transitions.

Installation
--
The repos config is set for a local environment. Edit the app database config according to your local settings and make sure you have run the following SQL. 

    CREATE DATABASE `carrera-air` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
    USE `carrera-air`;

    CREATE TABLE `entries` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `email` text,
      `phone` text,
      `firstname` varchar(25) DEFAULT NULL,
      `lastname` text,
      `address` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      PRIMARY KEY (`id`)
     ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;
  
This app is developed and tested using Apache, MySQL and PHP 5.3.6. Details regarding the CakePHP Framework is found at http://cakephp.org/

After editing the db config (found in app/Config/database.php) and creating the database with the above code you are good to go, just fire up your server and presto!

Application views
-- 

![Screenshot 01](http://marcusblom.se/carrera/01.png)

Above is the view served to the user when visiting the application.

![Screenshot 01](http://marcusblom.se/carrera/02.png)

Validation of the form fields are performed by Cake. The user is prompted if the fields are not input correctly.

![Screenshot 01](http://marcusblom.se/carrera/03.png)

When the user have entered valid data the information is written to the database and the user is presented with a "Good luck!"-page. The brief contained to information regarding how many times a user can enter so for now users can enter as many times as they wish. An improvement is to validate against the database as well or set a time limit to get cleaner data.

![Screenshot 01](http://marcusblom.se/carrera/04.png)

visit the appurl/entries to view a overview of the entered data. You can delete of edit data from here. In production this view must be either removed or improved with authentication.

![Screenshot 01](http://marcusblom.se/carrera/05.png)

This is the view presented when viewing a entry details.

![Screenshot 01](http://marcusblom.se/carrera/06.png)

This view is presented when editing a entry data.

Contact
--

For any questions regarding the site, please contact me at marcus@insiktsfabriken.se
