<?php

class EntriesController extends AppController {
    public $helpers = array('Html', 'Form');

    public function index() {
        $this->set('entries', $this->Entry->find('all'));
    }

		public function view($id) {
		        $this->Entry->id = $id;
		        $this->set('entry', $this->Entry->read());
		    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Entry->create();
            if ($this->Entry->save($this->request->data)) {
                $this->Session->setFlash('Your entry has been saved.');
                $this->redirect(array('action' => 'thank_you'));
            } else {
                $this->Session->setFlash('Unable to save your entry.');
            }
        }
    }

		public function thank_you() {
		    }

		public function edit($id = null) {
		    $this->Entry->id = $id;
		    if ($this->request->is('get')) {
		        $this->request->data = $this->Entry->read();
		    } else {
		        if ($this->Entry->save($this->request->data)) {
		            $this->Session->setFlash('Your entry has been updated.');
		            $this->redirect(array('action' => 'index'));
		        } else {
		            $this->Session->setFlash('Unable to update your entry.');
		        }
		    }
		}

		public function delete($id) {
		    if ($this->request->is('get')) {
		        throw new MethodNotAllowedException();
		    }
		    if ($this->Entry->delete($id)) {
		        $this->Session->setFlash('The entry with id: ' . $id . ' has been deleted.');
		        $this->redirect(array('action' => 'index'));
		    }
		}
}

?>