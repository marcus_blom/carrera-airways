<body class="preload">

	<script>
			$(document).ready(function(){

				$('BODY').bgStretcher({
					images: ['img/bg0.jpg', 'img/bg1.jpg', 'img/bg2.jpg', 'img/bg3.jpg'],
					imageWidth: 1920, 
					imageHeight: 1080, 
					slideDirection: 'N',
					slideShowSpeed: 1000,
					transitionEffect: 'fade',
					sequenceMode: 'normal',
					buttonPrev: '#prev',
					buttonNext: '#next',
					pagination: '#nav',
					anchoring: 'left center',
					anchoringImg: 'left center'
				});

			});
	</script>	
	
	<div class="container">
		<div class="sixteen columns animatedHeader bounceInDown" id="header">
			<div class="twelve columns alpha">
				<h1>Semester.</h1>
			</div>
			<div class="four columns omega" id="logo"></div>
		</div>
		<div class="eight columns container-bg animatedMessage fadeIn">
			<div class="message">
			<h2>Byt ut vardagens kyliga och snöblandade regn mot strandnära hotell och solsäng</h2>
				<p>
Vore det inte skönt med god mat, strålande sol och 25 grader plus istället för minus? Vore det inte härligt med semester? Det tycker vi på Carrera Airways också. Därför vill vi att du skall få chansen att vinna en fantastisk semester. Det enda du behöver göra är att fylla i formuläret och trycka på knappen för att tävla. Vi lottar ut en vinnare som får åka till ett av Carrera Airways charterdestinationer under vecka 9. Du får ta med dig hela familen! Tävlingen pågår till och med 31a december 2012. </br></br>Ta chansen, stranden väntar på dig.
<h4>Fylla bara i formuläret så kan du vinna en semester för hela familjen!</h4>
				</p>
			</div>
		</div>

		<div class="eight columns animatedForm bounceInLeft">
			<h3>Vinn en semester från Carrera Airways!</h3>
			<ul class="square">
				<?php
				echo $this->Form->create('Entry');
				echo $this->Form->label('firstname', 'Förnamn');
				echo $this->Form->input('firstname', array('type' => 'text', 'label' => false));
				echo $this->Form->label('lastname', 'Efternamn');
				echo $this->Form->input('lastname', array('type' => 'text', 'label' => false));
				echo $this->Form->label('email', 'Email');
				echo $this->Form->input('email', array('type' => 'text', 'label' => false));
				echo $this->Form->label('phone', 'Telefonnummer');
				echo $this->Form->input('phone', array('type' => 'text', 'label' => false));
				echo $this->Form->label('address', 'Adress');
				echo $this->Form->input('address', array('rows' => '4', 'label' => false));
				echo $this->Form->submit('Tävla!', array('class' => 'animatedButton tada'));
				echo $this->Form->end();

				?>
			</ul>
		</div>
	
		<div class="sixteen columns animatedFooter bounceInUp" id="footer">
			http://www.carrerairways.se/
		</div>
		
	</div><!-- container -->
</body>
</html>