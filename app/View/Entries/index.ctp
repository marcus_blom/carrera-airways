<body
><div class="container">
	<div class="sixteen columns">
		<h1>Alla tävlande</h1>
		<div class="edit-bg">
		<h2><?php echo $this->Html->link('Lägg till tävlande', array('controller' => 'entries', 'action' => 'add')); ?></h2>
		<table>
		    <tr>
		        <th>Förnamn</th>
		        <th>Efternamn</th>
		        <th>Telefon</th>
		        <th>Email</th>
		        <th>Adress</th>
		        <th>Skapad</th>
		        <th>Ändra</th>
		    </tr>

		    <!-- Here is where we loop through our $posts array, printing out post info -->

		    <?php foreach ($entries as $entry): ?>
		    <tr>
		        <td>
			        <?php echo $this->Html->link($entry['Entry']['firstname'],
					array('controller' => 'entries', 'action' => 'view', $entry['Entry']['id'])); ?>
				</td>
		        <td>
			        <?php echo $this->Html->link($entry['Entry']['lastname'],
					array('controller' => 'entries', 'action' => 'view', $entry['Entry']['id'])); ?>
				</td>
		        <td><?php echo $entry['Entry']['phone']; ?></td>
		        <td><?php echo $entry['Entry']['email']; ?></td>
		        <td><?php echo $entry['Entry']['address']; ?></td>
		        <td><?php echo $entry['Entry']['created']; ?></td>
				<td>
				<?php echo $this->Html->link('Redigera', array('action' => 'edit', $entry['Entry']['id'])); ?>
				<?php echo $this->Form->postLink('Ta bort',
		              array('action' => 'delete', $entry['Entry']['id']),
		              array('confirm' => 'Är du säker?'));
		          ?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
		    <?php unset($entry); ?>
		</table>
	</div>
	</div>
</div><!-- container -->
</body>
