<body>

	<script>
		// Det här scriptet startar bgStretcher som hanterar bakgrundsbilderna //

			$(document).ready(function(){

				$('BODY').bgStretcher({
					images: ['img/bg0.jpg', 'img/bg1.jpg', 'img/bg2.jpg', 'img/bg3.jpg'],
					imageWidth: 1920, 
					imageHeight: 1080, 
					slideDirection: 'N',
					slideShowSpeed: 1000,
					transitionEffect: 'fade',
					sequenceMode: 'normal',
					buttonPrev: '#prev',
					buttonNext: '#next',
					pagination: '#nav',
					anchoring: 'left center',
					anchoringImg: 'left center'
				});

			});
	</script>

	<div class="container">
		<div class="sixteen columns" id="header">
			<h1 class="centertext">Härligt! Lycka till!</h1>
		</div>
	</div><!-- container -->
</body>
</html>