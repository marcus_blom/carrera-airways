<body
><div class="container">
	<div class="eight columns">
		<h1>Redigera</h1>
		<?php
			echo $this->Form->create('Entry', array('action' => 'edit'));
			echo $this->Form->input('firstname', array('type' => 'text', 'label' => 'Förnamn'));
			echo $this->Form->input('lastname', array('type' => 'text', 'label' => 'Efternamn'));
			echo $this->Form->input('email', array('type' => 'text', 'label' => 'Email'));
			echo $this->Form->input('phone', array('type' => 'text', 'label' => 'Telefonnummer'));
			echo $this->Form->input('address', array('rows' => '3', 'label' => 'Adress'));
			echo $this->Form->end('Spara');
		?>
	</div>
</div><!-- container -->
</body>
