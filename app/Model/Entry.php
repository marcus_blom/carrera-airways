<?php 

class Entry extends AppModel {

  public $validate = array(

      'firstname' => array(
          'rule' => 'notEmpty',
          'message' => 'Fyll i ditt förnamn.'
      ),
      'lastname' => array(
          'rule' => 'notEmpty',
          'message' => 'Fyll i ditt efternamn.'
      ),
	    'email' => array(
	        'rule' => 'email',
          'message' => 'Fyll i en fullständig emailadress.'
	    ),
	    'address' => array(
	        'rule' => 'notEmpty',
          'message' => 'Fyll i din fullständiga adress.'
	    ),
			'phone' => array(
          'rule' => 'notEmpty',
          'message' => 'Fyll i ditt telefonnummer.'
      ),
  );

}

?>